from gcloud import storage
from oauth2client.service_account import ServiceAccountCredentials
import os

credentials_dict = {
    'type': 'service_account',
    'client_id': os.environ['BACKUP_CLIENT_ID'],
    'client_email': os.environ['BACKUP_CLIENT_EMAIL'],
    'private_key_id': os.environ['BACKUP_PRIVATE_KEY_ID'],
    'private_key': os.environ['BACKUP_PRIVATE_KEY'],
}
credentials = ServiceAccountCredentials.from_json_keyfile_dict(
    credentials_dict
)

print(os.environ['BACKUP_CLIENT_EMAIL'])

#client = storage.Client(credentials=credentials, project='hashmap-usaa-testing')
client = storage.Client()
def _write_file_to_gcs(file_path: str) -> None:
    # Instantiates a client
    storage_client = storage.Client()
    if os.path.isfile(file_path):
        print('Getting bucket')
        bucket = storage_client.get_bucket(BUCKET_NAME)
        print('Getting blob')
        blob = bucket.blob(os.path.basename(file_path))
        print('Uploading from Filename')
        blob.upload_from_filename(file_path)
        
bucket = client.get_bucket('hashmap-usaa-testing')
blob = bucket.blob('steps.txt')
blob.upload_from_filename('steps.txt')